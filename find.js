function find(elements, cb) {
  //created function
  if (!Array.isArray(elements)) {
    //checking elements are array or not
    return "Not an Array";
  }
  for (let index = 0; index < elements.length; index++) {
    //for loop to iterate inside array
    if (cb(elements[index])) {
      //calling cb function
      return elements[index]; //value will be returned if condition returns true
    }
  }
  return `undefined`;
}
module.exports = find; //exporting function
