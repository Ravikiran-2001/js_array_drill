function filter(elements, cb) {
  //created function
  if (!Array.isArray(elements)) {
    //checking elements are array or not
    return "Not an Array";
  }
  let newArray = []; //created new array
  for (let index = 0; index < elements.length; index++) {
    //for loop to iterate inside array
    if (cb(elements[index])) {
      //calling cb function
      newArray.push(elements[index]); //value will be pushed to new array if cb function returns true
    }
  }
  return newArray;
}
module.exports = filter; //exporting filter function
