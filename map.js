function map(elements, cb) {
  //created function
  if (!Array.isArray(elements)) {
    //checking elements are array or not
    return "Not an array";
  }
  let newArray = []; //created new array
  for (let index = 0; index < elements.length; index++) {
    //iterating inside array
    newArray.push(cb(elements[index])); //calling cb function and pushing returned value inside new array
  }
  return newArray;
}
module.exports = map; //exports function
