function each(elements, cb) {
  //created function
  if (!Array.isArray(elements)) {
    //checking elements are array or not
    console.log("Not an array");
    return;
  }
  for (let index = 0; index < elements.length; index++) {
    //for loop to iterate inside array
    cb(elements[index], index); //calling cb function
  }
}
module.exports = each; //exporting each function
