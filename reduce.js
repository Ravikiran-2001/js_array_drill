function reduce(elements, cb, startingValue) {
  //created function
  if (!Array.isArray(elements)) {
    //checking elements are array or not
    return "Not an array";
  }
  if (startingValue === undefined) {
    //checking startingValue is defined or not
    startingValue = elements[0]; //if not defined, initilizing variable with index 0 value of elements
    for (let index = 1; index < elements.length; index++) {
      //for loop to iterate inside array
      startingValue = cb(startingValue, elements[index]); //calling cb function and storing returned value in startingValue
    }
  } else {
    //if value is initialized before
    for (let index = 0; index < elements.length; index++) {
      startingValue = cb(startingValue, elements[index]);
    }
  }
  return startingValue;
}
module.exports = reduce; //exporting function
