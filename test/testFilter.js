const filter = require("../Js_Array_Drill_1/filter"); //importing function
let numbers = require("../Js_Array_Drill_1/array"); //importing array

function filteringOut(value) {
  //created function
  return value > 2; //checking value is greater than 2 or not
}
console.log(filter(numbers, filteringOut)); //calling and printing returned value of filter function
