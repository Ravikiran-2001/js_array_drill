const each = require("../Js_Array_Drill_1/each"); //importing function
let numbers = require("../Js_Array_Drill_1/array"); //importing array

function printValueAndIndex(value, index) {
  //created function
  console.log(`value : ${value}, index no. : ${index}`); //printing given value from each function
}
each(numbers, printValueAndIndex); //calling each function
