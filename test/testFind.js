const find = require("../Js_Array_Drill_1/find"); //importing function
const numbers = require("../Js_Array_Drill_1/array"); //importing array

function greaterThanThree(value) {
  //created function
  return value > 3; //checking value is greater than 3 or not
}
console.log(find(numbers, greaterThanThree)); //calling and printing returned value of find function
