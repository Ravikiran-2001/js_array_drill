const reduce = require("../Js_Array_Drill_1/reduce"); //importing function
let numbers = require("../Js_Array_Drill_1/array"); //importing array

function addition(startingValue, currValue) {
  //created function
  return startingValue + currValue; //returning addition of startingValue and currValue
}
console.log(reduce(numbers, addition)); //calling and printing returned value of reduce function
