const map = require("../Js_Array_Drill_1/map"); //importing function
const numbers = require("../Js_Array_Drill_1/array"); //importing array

function square(value) {
  //created function
  return value * value; //returning square of given value
}
console.log(map(numbers, square)); //calling and printing returned value of map function
