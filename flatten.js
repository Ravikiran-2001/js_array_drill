function flatten(elements) {
  //created function
  if (!Array.isArray(elements)) {
    //checking elements are array or not
    return "Not an Array";
  }
  let newArray = []; //created new array
  function recursiveFunction(elements2) {
    //created a function
    for (let index = 0; index < elements2.length; index++) {
      //for loop to iterate inside an array
      if (Array.isArray(elements2[index])) {
        //if element is array it will again call the function
        recursiveFunction(elements2[index]);
      } else {
        newArray.push(elements2[index]); //if element is not an array it will be pushed to new array
      }
    }
  }
  recursiveFunction(elements); //calling function
  return newArray;
}
module.exports = flatten; //exporting function
